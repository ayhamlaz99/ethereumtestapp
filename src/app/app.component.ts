import { Component } from '@angular/core';
import { EthereumService } from './services/ethereum.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'EthereumApp';

  lastBlockNumber!: number;
  transactionCount!: number;
  address: string = '0xdAC17F958D2ee523a2206206994597C13D831ec7';

  constructor(private srv: EthereumService) { }

  ngOnInit() {
  }

  getLastBlockNumber() {
    this.srv.getLastBlockNumber<number>()
    .subscribe((blockNumber) => {
      this.lastBlockNumber = blockNumber;
    }, (error) => {
      console.error('Error:', error)
    });
  }

  getTransactionCount() {
    this.srv.getTransactionCount<number>(this.address)
      .subscribe((count) => {
        this.transactionCount = count;
      }, (error) => {
        console.error('Error:', error);
      });
  }

}
