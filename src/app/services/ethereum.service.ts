import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import Web3 from 'web3';

@Injectable({
  providedIn: 'root'
})
export class EthereumService {
  private web3: Web3;

  constructor() {
    this.web3 = new Web3('https://mainnet.infura.io/v3/187f4e7f0ef949f283e0dd8e3c228ac0');
  }

  getLastBlockNumber<T>(): Observable<T> {
    return from(this.web3.eth.getBlockNumber()) as Observable<T>;
  }

  getTransactionCount<T>(address: string): Observable<T> {
    return from(this.web3.eth.getTransactionCount(address)) as Observable<T>;
  }
}
