import { TestBed } from '@angular/core/testing';
import { EthereumService } from './ethereum.service';
import Web3 from 'web3';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

describe('EthereumService', () => {
  let service: EthereumService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EthereumService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the last block number', (done: DoneFn) => {
    service.getLastBlockNumber<number>().subscribe((blockNumber: number) => {
      expect(blockNumber).toBeGreaterThan(0);
      done();
    });
  });

  it('should get the transaction count', (done: DoneFn) => {
    const address = '0xdAC17F958D2ee523a2206206994597C13D831ec7';
    service.getTransactionCount<number>(address).subscribe((count: number) => {
      expect(count).toBeGreaterThanOrEqual(0);
      done();
    });
  });
});