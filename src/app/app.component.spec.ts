import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { EthereumService } from './services/ethereum.service';
import { of } from 'rxjs';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let ethereumService: jasmine.SpyObj<EthereumService>;

  beforeEach(async () => {
    const ethereumServiceSpy = jasmine.createSpyObj('EthereumService', ['getLastBlockNumber', 'getTransactionCount']);

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      declarations: [AppComponent],
      providers: [{ provide: EthereumService, useValue: ethereumServiceSpy }]
    }).compileComponents();

    ethereumService = TestBed.inject(EthereumService) as jasmine.SpyObj<EthereumService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should get the last block number', () => {
    const blockNumber = 12345;
    ethereumService.getLastBlockNumber.and.returnValue(of(blockNumber));

    component.getLastBlockNumber();

    expect(ethereumService.getLastBlockNumber).toHaveBeenCalled();
    expect(component.lastBlockNumber).toEqual(blockNumber);
  });

  it('should get the transaction count', () => {
    const transactionCount = 100;
    ethereumService.getTransactionCount.and.returnValue(of(transactionCount));

    component.getTransactionCount();

    expect(ethereumService.getTransactionCount).toHaveBeenCalledWith(component.address);
    expect(component.transactionCount).toEqual(transactionCount);
  });
});